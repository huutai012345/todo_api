const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

mongoose.connect(
  "mongodb+srv://NHT:gQEukhZ05cPbYpbD@cluster0.rbq8v.mongodb.net/blog?retryWrites=true&w=majority",
  { useNewUrlParser: true, useUnifiedTopology: true },
  (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("Success");
    }
  }
);

app.use(bodyParser.json());
app.use(require("./Routes/Todo"));

app.listen(3000, () => {
  console.log("Server is running in port 3000");
});
