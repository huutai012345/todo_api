const express = require("express");
const router = express.Router();
const Post = require("../Models/Post");

router.post("/todo", function (req, res) {
  let post = new Post({
    content: req.body.content,
  });
  post.save(function (err, post) {
    if (err) res.json({ message: err });
    res.json(post);
  });
});

router.get("/todos", function (req, res) {
  Post.find(function (err, items) {
    if (err) {
      res.json({ message: err });
    } else {
      res.json({ todo: items });
    }
  });
});

router.get("/", function (req, res) {
  res.json({ message: "Hello" });
});

module.exports = router;
